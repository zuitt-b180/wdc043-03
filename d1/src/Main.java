import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Loops are control structures that allow code blocks to be repeated according to conditions set.

        // Type of Loops
            // For Loop
            // While Loop
            // Do-While Loop

        // While Loop
        int a = 1;
        while (a < 5) {
            System.out.println("While Loop Counter: " + a);
            a++;
        }
        System.out.println("---- Do-While Loops --------");
        // Do While Loop
        int b = 12;
        do {
            System.out.println("Do-While Countdown: " + b);
            b--;
        } while (b > 10);

        System.out.println("------------");

        Scanner appScanner = new Scanner(System.in);
        String name = "";

        // isBlank() is a String Method, which checks the non-whitespace characters in our string and will return true even if there is only a blank/whitespace; returns a boolean.
            // System.out.println(name.isEmpty()); // will return False if there is at least whitespace in the string
            // System.out.println(name.isBlank()); // will return True, if there is no characters or if the length of the string is 0, AND if the string contains a whitespace
        while (!name.isBlank()) { // added ! to stop running this loop
            System.out.println("What is your name?");
            name = appScanner.nextLine();


            if (!name.isBlank()) {  // to read this: "if name is NOT blank"
                System.out.println("Hi " + name + "!");
            }
        }

        System.out.println("----- For Loops -------");

        // For Loops - are more versatile than our while loops
        for (int i = 1; i <= 10; i++) {
            System.out.println("Count " + i);
        }

        System.out.println("----- Java Array -------");
        // For loop over Java Array
        int[] intArray = {100, 200, 300, 400, 500};
        for (int i = 0; i < intArray.length; i++) {
            System.out.println("Item Index " + intArray[i]);
        }

        System.out.println("----- Loop over Multi-dimensional Array -------");
        // Loop over Multi-dimensional Array
        // Multi-dimensional Arrays
            // A two-dimensional array, which can best be described by two lengths nested within each other like a matrix.
            // The first array could be for rows, the second could be for cols.
        String[][] classroom = new String[3][3]; // [outerArray][innerArray]

        // First Row
        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        // Second Row
        classroom[1][0] = "Sora";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        // Third Row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        System.out.println(Arrays.toString(classroom));

        // Nested For Loop
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.println(classroom[row][col]);
            }
        }

        System.out.println("----- Enhanced For Loop for Java Array/ArrayList -------");
        // Enhanced For Loop for Java Array/ArrayList
        // In Java, a for-each loop can be used to iterate over the items of an array and arraylist.
        // for-each in java for array and arraylist is also called enhanced for loop.
        /*
            for(DataType parameterName: arrayName) {
                // code block will for each item in the array
            }
        */

        String[] members = {"Eugene", "Vincent", "Dennis", "Alfred"};
        for (String member: members ) { // member is a parameter which will represent each item in the given array
            System.out.println(member);
        }

        System.out.println("----- Multidimensional Array ForEach -------");
        // Multidimensional Array for-each
        for (String[] row: classroom) {
            // row = array
            for (String student: row) {
                System.out.println(student);
            }
        }

        // HashMap forEach
            // HashMap has a method for itertaing each field-value pair.
            // the HashMap forEach() requires a lambda expression as an argument.
            // A lambda expression in Java, is a short block of code which takes in parameters and returns a value. Lambda expressions are similar to methods, but they do not need a name and they can be implemented within another method.

        System.out.println("----- HashMaps -------");
        HashMap<String, String> techniques = new HashMap<>();
        techniques.put(members[0], "Spirit Gun");
        techniques.put(members[1], "Black Dragon");
        techniques.put(members[2], "Rose Whip");
        techniques.put(members[3], "Spirit Sword");
        System.out.println(techniques);

        techniques.forEach((key, value) -> {
            System.out.println("Member  " + key + " uses " + value);
        });

        // Exceptional Handling (Scanner)
        // As the Scanner methods have specific dataTypes associated with them (nextLine(), nextInt(), nextDouble()), it is best to assume that there will be issues in user input especially if they fail to provide correct data type.
        // Exception Handling refers to managing and catching run-time errors in order to safely run our code.

        System.out.println("Enter an Integer: ");
        int num = 0;

        // try-catch-finally
        try {
            num = appScanner.nextInt();
        } catch (Exception err) { // 'err' or just 'e'
            System.out.println("Invalid Input");
            err.printStackTrace(); // this is to track where error happened
        }
        System.out.println(num);
    }
}