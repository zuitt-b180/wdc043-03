import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

    // HASHMAP
        String[] games = {"Mario Odyssey", "Super Smash Bros. Ultimate", "Luigi's Mansion 3", "Pokemon Sword", "Pokemon Shield"};

        HashMap<String, Integer> gameStocks = new HashMap<>();
        gameStocks.put(games[0], 50);
        gameStocks.put(games[1], 20);
        gameStocks.put(games[2], 15);
        gameStocks.put(games[3], 30);
        gameStocks.put(games[4], 100);
        // System.out.println(gameStocks);

        gameStocks.forEach((key, value) -> {
            System.out.println(key + " has " + value + " left.");
        });

        ArrayList<String> topGames = new ArrayList<>();

        gameStocks.forEach((key, value) -> {
            if (value <= 30) {
                topGames.add(key);
                System.out.println(key + " has been added to the Top Game List!");
            }
        });

        System.out.println(topGames);

    }
}